package main

import (
	"context"
	"flag"
	"net"
	"net/http"
	"time"
	"github.com/gogf/gf/os/glog"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
	"google.golang.org/grpc/resolver"

	pb "github.com/wwcd/grpc-lb/cmd/helloworld"
	grpclb "github.com/wwcd/grpc-lb/etcdv3"
)

var (
	svc  = flag.String("service", "hello_service", "service name")
	host = flag.String("host", "192.168.0.101", "listening host")
	port = flag.String("port", "60001", "listening port")
	reg  = flag.String("reg", "http://192.168.0.101:2379", "register etcd address")
)

func main() {
	flag.Parse()
	r := grpclb.NewResolver(*reg, *svc)
	resolver.Register(r)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	// https://github.com/grpc/grpc/blob/master/doc/naming.md
	// The gRPC client library will use the specified scheme to pick the right resolver plugin and pass it the fully qualified name string.
	conn, err := grpc.DialContext(ctx, r.Scheme()+"://authority/"+*svc, grpc.WithInsecure(), grpc.WithBalancerName(roundrobin.Name), grpc.WithBlock())
	cancel()
	if err != nil {
		panic(err)
	}

	mux := runtime.NewServeMux()
	err = pb.RegisterGreeterHandler(ctx, mux, conn)
	if err != nil {
		panic(err)
	}

	// Start HTTP server (and proxy calls to gRPC server endpoint)
	glog.Fatal(http.ListenAndServe(net.JoinHostPort(*host, *port), mux))
}
